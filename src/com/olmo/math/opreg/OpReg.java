package com.olmo.math.opreg;

public abstract class OpReg {
	public enum opRegTypes {constant, plus, xor, and, rightRotate, rightShift, not, symbBit, symbBitSet};
	opRegTypes type;
	
	public OpReg(opRegTypes t){
		type=t;
	}
	public abstract void getOp(StringBuffer sb) throws Exception;
}
