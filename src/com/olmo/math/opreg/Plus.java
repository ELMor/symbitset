package com.olmo.math.opreg;

public class Plus extends BiOperand {

	public Plus(OpReg o1, OpReg o2) {
		super(opRegTypes.plus);
		op1=o1;
		op2=o2;
	}

	@Override
	public void getOp(StringBuffer sb) throws Exception {
		sb.append("(");
		op1.getOp(sb);
		sb.append("+");
		op2.getOp(sb);
		sb.append(")");
	}

}
