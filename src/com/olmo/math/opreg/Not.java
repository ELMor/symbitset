package com.olmo.math.opreg;

public class Not extends MonoOperand {
	
	public Not(OpReg sb) {
		super(opRegTypes.not);
		op=sb;
	}

	@Override
	public void getOp(StringBuffer sb) throws Exception {
		sb.append("!(");
		op.getOp(sb);
		sb.append(")");
	}

}
