package com.olmo.math.opreg;

public class Constante extends MonoOperand {

	public Constante(OpReg o) {
		super(opRegTypes.constant);
		op=o;
	}

	@Override
	public void getOp(StringBuffer sb) {
		sb.append("K");
	}

}
