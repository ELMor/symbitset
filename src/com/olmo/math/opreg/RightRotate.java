package com.olmo.math.opreg;

public class RightRotate extends MonoOperandCons {

	public RightRotate(OpReg o1, long o2) {
		super(opRegTypes.rightRotate);
		op=o1;
		val=o2;
	}

	@Override
	public void getOp(StringBuffer sb) throws Exception {
		op.getOp(sb);
		sb.append(">>").append(val);
	}

}
