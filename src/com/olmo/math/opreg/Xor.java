package com.olmo.math.opreg;

public class Xor extends BiOperand {

	public Xor(OpReg o1, OpReg o2) {
		super(opRegTypes.xor);
		op1=o1;
		op2=o2;
	}
	
	@Override
	public void getOp(StringBuffer sb) throws Exception {
		sb.append("(");
		op1.getOp(sb);
		sb.append("^");
		op2.getOp(sb);
		sb.append(")");
	}

}
