package com.olmo.math;

import java.util.Vector;

import com.olmo.math.opreg.And;
import com.olmo.math.opreg.Not;
import com.olmo.math.opreg.OpReg;
import com.olmo.math.opreg.Plus;
import com.olmo.math.opreg.RightRotate;
import com.olmo.math.opreg.RightShift;
import com.olmo.math.opreg.Xor;

public class SymBS extends OpReg {
	String name;
	Vector<SymB> value=new Vector<SymB>();
	OpReg opreg=null;
	
	public SymBS(String name, byte msg[]){
		this(name, (OpReg)null);
	}
	public SymBS(String name, long v){
		this(name,(OpReg)null);
		majorVal(v, 32);
	}
	public SymBS(String name, OpReg or){
		super(opRegTypes.symbBitSet);
		this.name=name;
		opreg=or;
	}
	public void majorVal(long  v,int sz){
		for(int i=sz-1;i>=0;i--){
			long nb=v>>i;
			if( (nb&0x1) == 1)
				addBit('t');
			else
				addBit('f');
		}
		
	}
	
	public void addBit(char c){
		int pos=value.size();
		SymB sb=new SymB(this, pos, c);
		value.add(sb);
	}
	
	public int size(){
		return value.size();
	}
	
	public SymBS getChunk(int pos, int size){
		SymBS ret=new SymBS(name+"["+pos+","+size+"]",(OpReg)null);
		for(int i=pos;i<pos+size;i++)
			ret.value.add(value.get(i));
		return ret;
	}
	
	public long getLongAt(int pos) throws Exception {
		long ret=0;
		for(int i=pos;i<pos+32;i++){
			ret=ret<<1;
			ret|=value.get(i).getVal();
		}
		return ret;
	}
	
	public byte getByteAt(int pos) throws Exception{
		byte ret=0;
		for(int i=pos;i<pos+8;i++){
			ret=(byte) (ret<<1);
			ret|=value.get(i).getVal();
		}
		return ret;
	}
	
	public byte[] getBytes() throws Exception {
		byte ret[]=new byte[size()/8];
		for(int i=0;i<ret.length;i++)
			ret[i]=getByteAt(i*8);
		return ret;
	}
	
	public SymBS rightRotate(int sizeOfRotate){
		SymBS ret=new SymBS(name, new RightRotate(this, sizeOfRotate));
		int sz=value.size();
		sizeOfRotate=sizeOfRotate%sz;
		int beginIndex=sz-sizeOfRotate;
		for(int i=0;i<sz;i++){
			SymB ib=value.get(beginIndex).clon();
			ret.value.add(ib);
			beginIndex++;
			if(beginIndex>=sz)
				beginIndex=0;
		}
		return ret;
	}
	
	public SymBS rightShift(int sizeOfShift){
		SymBS ret=new SymBS(name, new RightShift(this, sizeOfShift));
		int sz=value.size();
		for(int i=0;i<sizeOfShift;i++)
			ret.addBit('f');
		for(int i=0;i<sz-sizeOfShift;i++)
			ret.value.add(value.get(i).clon());
		return ret;
	}
	public SymBS xor(SymBS other){
		SymBS ret=new SymBS(null,new Xor(this, other));
		for(int i=0;i<value.size();i++){
			SymB sb=value.get(i).xor(other.value.get(i));
			ret.value.add(sb);
		}
		return ret;
	}
	public SymBS plus(SymBS other) throws Exception {
		SymBS ret=new SymBS(null,new Plus(this,other));
		boolean carry=false;
		for(int i=size()-1;i>=0;i--){
			SymB otb=other.value.get(i);
			otb.carry=carry;
			SymB thb=value.get(i);
			SymB res=thb.add(otb);
			ret.value.add( 0, res );
			carry=res.carry;
		}
		return ret;
	}
	
	public SymBS not(){
		SymBS ret=new SymBS(null,new Not(this));
		for(int i=0;i<size();i++){
			ret.value.add(value.get(i).not());
		}
		return ret;
	}
	
	public SymBS and(SymBS other){
		SymBS ret=new SymBS(null,new And(this, other));
		for(int i=0;i<size();i++){
			ret.value.add(
					value.get(i).and(other.value.get(i))
					);
		}
		return ret;
	}
	
	public String convertToHex() throws Exception{
		String ret="";
		for(int i=0;i<value.size();i+=4){
			int nibble=0;
			for(int j=i;j<i+4;j++){
				nibble<<=1;
				nibble|=value.get(j).getVal();
			}
			char nib="0123456789abcdef".charAt(nibble);
			ret+=nib;
		}
		return ret;
	}
	@Override
	public void getOp(StringBuffer sb) throws Exception{
		if(opreg!=null){
			opreg.getOp(sb);
			sb.append("=");
		}
		//sb.append((name!=null?name:"")).append("=").append(convertToHex());
		sb.append(convertToHex());
	}
}

