package com.olmo.math;

import com.olmo.math.opreg.OpReg;

/*
 * SHA256("")
 * 0x e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
 * 
 * SHA256("The quick brown fox jumps over the lazy dog")
 * 0x d7a8fbb307d7809469ca9abcb0082e4f8d5651e46d3cdb762d02d0bf37c9e592
 * 
 * 
 * SHA256("The quick brown fox jumps over the lazy dog.")
 * 0x ef537f25c895bfa782526529a9b63d97aa631564d5d789c2b765448c8635fb6c
 *
 *hello
  2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824 (first round of sha-256)
  9595c9df90075148eb06860365df33584b75bff782a510c6cd4883a419833d50 (second round of sha-256)
 */

public class SHA256 {
	SymBS h0,h1,h2,h3,h4,h5,h6,h7,a,b,c,d,e,f,g,h;
	SymBS k[]=new SymBS[64];
	SymBS msg=new SymBS("MensajeOriginal",(OpReg)null);
	SymBS sha256;
	byte mensaje[];
	
	public SHA256(byte cMsg[]) throws Exception{
		mensaje=cMsg;
		h0=new SymBS("h0",(OpReg)null); 
		h1=new SymBS("h1",(OpReg)null); 
		h2=new SymBS("h2",(OpReg)null); 
		h3=new SymBS("h3",(OpReg)null); 
		h4=new SymBS("h4",(OpReg)null); 
		h5=new SymBS("h5",(OpReg)null); 
		h6=new SymBS("h6",(OpReg)null); 
		h7=new SymBS("h7",(OpReg)null); 
		h0.majorVal(SHA256Constants.hVal[0], 32);
		h1.majorVal(SHA256Constants.hVal[1], 32);
		h2.majorVal(SHA256Constants.hVal[2], 32);
		h3.majorVal(SHA256Constants.hVal[3], 32);
		h4.majorVal(SHA256Constants.hVal[4], 32);
		h5.majorVal(SHA256Constants.hVal[5], 32);
		h6.majorVal(SHA256Constants.hVal[6], 32);
		h7.majorVal(SHA256Constants.hVal[7], 32);
		
		for(int i=0;i<64;i++){
			k[i]=new SymBS("k"+i+"]",(OpReg)null);
			k[i].majorVal(SHA256Constants.kVal[i],32);
		}
		
		for(int i=0;i<cMsg.length;i++)
			msg.majorVal(cMsg[i], 8);
		
		//Append 1 bit
		msg.majorVal(0x80, 8);
		//Append until mod=448
		while(msg.size()%512 != 448)
			msg.majorVal(0, 8);
		//Append size of
		msg.majorVal(cMsg.length*8, 64);
		//Begin calc
		SymBS w[]=new SymBS[64];
		for(int chunk=0;chunk<msg.size();chunk=chunk+512){
			for(int i=0;i<16;i++){
				w[i]=new SymBS("w"+i+"]",(OpReg)null);
				w[i].majorVal(msg.getLongAt(chunk+i*32), 32);
			}
			for(int i=16;i<64;i++){
				SymBS s0= 
						w[i-15].rightRotate(7).xor(
						w[i-15].rightRotate(18).xor(
						w[i-15].rightShift(3))
						);
				SymBS s1=
						w[i-2].rightRotate(17).xor(
						w[i-2].rightRotate(19).xor(
						w[i-2].rightShift(10))
						);
				w[i]=w[i-16].plus(s0).plus(w[i-7]).plus(s1);
			}
		}
		a=new SymBS("a",(OpReg)null); a.majorVal(SHA256Constants.hVal[0], 32);
		b=new SymBS("b",(OpReg)null); b.majorVal(SHA256Constants.hVal[1], 32);
		c=new SymBS("c",(OpReg)null); c.majorVal(SHA256Constants.hVal[2], 32);
		d=new SymBS("d",(OpReg)null); d.majorVal(SHA256Constants.hVal[3], 32);
		e=new SymBS("e",(OpReg)null); e.majorVal(SHA256Constants.hVal[4], 32);
		f=new SymBS("f",(OpReg)null); f.majorVal(SHA256Constants.hVal[5], 32);
		g=new SymBS("g",(OpReg)null); g.majorVal(SHA256Constants.hVal[6], 32);
		h=new SymBS("h",(OpReg)null); h.majorVal(SHA256Constants.hVal[7], 32);
		for(int i=0;i<64;i++){
			SymBS S1= e.rightRotate(6).xor(e.rightRotate(11)).xor(e.rightRotate(25));
			SymBS ch= e.and(f).xor(e.not().and(g));
			SymBS temp1=h.plus(S1).plus(ch).plus(k[i]).plus(w[i]);
			SymBS S0= a.rightRotate(2).xor(a.rightRotate(13)).xor(a.rightRotate(22));
			SymBS maj= ((a.and(b)).xor(a.and(c))).xor(b.and(c));
			SymBS temp2=S0.plus(maj);
			h=g;
			g=f;
			f=e;
			e=d.plus(temp1);
			d=c;
			c=b;
			b=a;
			a=temp1.plus(temp2);
		}
		a=a.plus(h0);
		b=b.plus(h1);
		c=c.plus(h2);
		d=d.plus(h3);
		e=e.plus(h4);
		f=f.plus(h5);
		g=g.plus(h6);
		h=h.plus(h7);
	}
	public void print() throws Exception{
		System.out.print("0x");
		System.out.print(a.convertToHex());
		System.out.print(b.convertToHex());
		System.out.print(c.convertToHex());
		System.out.print(d.convertToHex());
		System.out.print(e.convertToHex());
		System.out.print(f.convertToHex());
		System.out.print(g.convertToHex());
		System.out.print(h.convertToHex());
		System.out.println("");
	}
	public byte[] getBytes() throws Exception {
		byte ret[]=new byte[32];
		System.arraycopy(a.getBytes(), 0, ret,  0, 4);
		System.arraycopy(b.getBytes(), 0, ret,  4, 4);
		System.arraycopy(c.getBytes(), 0, ret,  8, 4);
		System.arraycopy(d.getBytes(), 0, ret, 12, 4);
		System.arraycopy(e.getBytes(), 0, ret, 16, 4);
		System.arraycopy(f.getBytes(), 0, ret, 20, 4);
		System.arraycopy(g.getBytes(), 0, ret, 24, 4);
		System.arraycopy(h.getBytes(), 0, ret, 28, 4);		
		return ret;
	}
}
