package com.olmo.math;

import com.olmo.math.opreg.And;
import com.olmo.math.opreg.Constante;
import com.olmo.math.opreg.Not;
import com.olmo.math.opreg.OpReg;
import com.olmo.math.opreg.Plus;
import com.olmo.math.opreg.Xor;

public class SymB extends OpReg {
	char value='u';
	boolean carry;
	OpReg opreg=null;
	public SymBS owner;
	public int position;
	
	public SymB(SymBS group, int pos, char c){
		super(opRegTypes.symbBit);
		setVal(c);
		owner=group;
		position=pos;
		opreg=new Constante(owner);
	}
	
	public SymB clon(){
		SymB ret=new SymB(owner, position, value);
		ret.opreg=opreg;
		return ret;
	}
	
	public SymB(char c, OpReg or){
		super(opRegTypes.symbBit);
		value=c;
		carry=false;
		opreg=or;
	}

	public void setVal(char c){
		value=c=='t'?'t':(c=='f'?'f':'u');		
	}
	
	public int getVal() throws Exception{
		if(value=='u')
			throw new Exception("Unknow bit value");
		if(value=='f')
			return 0;
		return 1;
	}
	
	public SymB xor (SymB other){
		char c;
		if(value=='u' || other.value=='u')
			c='u';
		if(value==other.value)
			c='f';
		else
			c='t';
		return new SymB(c,new Xor(this, other));
	}
	
	public SymB add(SymB other) throws Exception{
		if(value=='u'||other.value=='u'){
			return new SymB('u',new Plus(this, other));
		}
		int res=getVal()+other.getVal()+(other.carry?1:0);
		SymB sb=null;
		switch(res){
		case 0: return new SymB('f',null);
		case 1: return new SymB('t',null);
		case 2: sb=new SymB('f',null); sb.carry=true; return sb;
		case 3: sb=new SymB('t',null); sb.carry=true;
		}
		return sb;
	}
	
	public SymB not(){
		if(value=='u')
			return new SymB('u',new Not(this));
		if(value=='f')
			return new SymB('t',null);
		return new SymB('f',null);
	}
	
	public SymB and(SymB other) {
		if(value=='f'||other.value=='f')
			return new SymB('f',null);
		if(value=='u'||other.value=='u')
			return new SymB('u', new And(this, other));
		return new SymB('t',null);
	}

	@Override
	public void getOp(StringBuffer sb) throws Exception {
		owner.getOp(sb);
		sb.append("[").append(position).append("]");
	}
	
}
